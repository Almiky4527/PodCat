import os

'''
Module for default configuration values related to this application.

In order to specify your own configuration, please use the ~/.podcat.json file. 
If user configuration file is not found or wasn't specified in a command, these
configuration values defined here will be used instead.

Refer to the structure of the 'CONFIGURATION_DEFAULTS' dictionary (key to value pairs)
to see the structure of your user config file.
'''


# Filepath separator: '\' for Windows, '/' for Linux.
# Userhome '~' doesn't work on Win, but os.expanduser resolves this accordingly.
PODCASTDIR_PATH = "~\\Podcasts" if os.name == "nt" else "~/Podcasts"
MAXDOWNLOADS = 1
MAXAGE_DAYS = 365

MIMETYPES_DEFAULTS = [
    "audio/aac",
    "audio/ogg",
    "audio/mpeg",
    "audio/x-mpeg",
    "audio/mp3",
    "audio/mp4",
    "video/mp4"
]

CONFIGURATION_DEFAULTS = {
    "podcast-directory": PODCASTDIR_PATH,
    "maxnum": MAXDOWNLOADS,
    "maxage-days": MAXAGE_DAYS,
    "mimetypes": MIMETYPES_DEFAULTS
}
