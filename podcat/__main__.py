#!/usr/bin/python3
"""
PodCat - podcatcher for terminal.

Usage:
    podcat import <feed-url> [<shortname>] [-c=<path>]
    podcat update [<shortname>] [-c=<path>]
    podcat feeds [-c=<path>]
    podcat episodes <shortname> [-c=<path>]
    podcat download [<shortname> --number=<n> --how-many=<n>] [-c=<path>]
    podcat rename <shortname> <new_name> [-c=<path>]
    podcat prune [<shortname> --number=<n> --maxage-days=<n>]

Options:
    -c --config=<path>   Specifies an alternate config file [default: ~/.podcat.json].
    -h --help            Shows this help message.
"""

from colorama import Fore, Back, Style
from docopt import docopt
from os.path import expanduser
from sys import exit
from time import mktime
from defaults import *
import colorama
import datetime
import feedparser
import json
import requests
import sys
import re

"""
RSS date-times follow RFC 2822, same as email headers. This is the chain of stackoverflow posts that
led me to believe this is true.

https://stackoverflow.com/questions/11993258/what-is-the-correct-format-for-rss-feed-pubdate
https://stackoverflow.com/questions/885015/how-to-parse-a-rfc-2822-date-time-into-a-python-datetime

feedparser 6.0.10 provides parsed dates: https://feedparser.readthedocs.io/en/latest/date-parsing.html
"""


CONFIGURATION = {}
CONFIGURATION.update(CONFIGURATION_DEFAULTS)


def print_err(err):
    print(
        Fore.RED + Style.BRIGHT + err + Fore.RESET + Back.RESET + Style.RESET_ALL,
        file=sys.stderr
    )


def print_green(string: str, end="\n"):
    print(Fore.GREEN + string + Fore.RESET, end=end)


def print_red(string: str, end="\n"):
    print(Fore.RED + string + Fore.RESET, end=end)


def get_folder(shortname) -> str:
    base = CONFIGURATION["podcast-directory"]
    return os.path.join(base, shortname)


def get_feed_file(shortname) -> str:
    return os.path.join(get_folder(shortname), "feed.json")


def get_filename_from_url(url: str) -> str:
    return url.split('/')[-1].split('?')[0]


def episode_too_old(episode, max_age) -> bool:
    now = datetime.datetime.utcnow()
    dt_published = datetime.datetime.fromtimestamp(episode["published"])
    return max_age and (now - dt_published > datetime.timedelta(days=max_age))


def sorted_episodes(episodes: list[dict], key="published", reverse=True) -> list[dict]:
    return sorted(episodes, key=lambda episode: episode[key], reverse=reverse)


def import_feed(url: str, shortname=""):
    """
    Creates a folder for the new feed, and then inserts a new feed.json
    that will contain all the necessary information about this feed, and
    all the episodes contained.
    """
    # configuration for this feed, will be written to file.
    feed = {}
    # get the feed.
    parsed_feed = feedparser.parse(url)

    if shortname:
        folder = get_folder(shortname)
    # if the user did not specify a folder name,
    # we have to create one from the title
    else:
        # the rss advertises a title, lets use that.
        if hasattr(parsed_feed["feed"], "title"):
            title = parsed_feed["feed"]["title"]
        # still no success, lets use the last part of the url
        else:
            title = url.rsplit('/', 1)[-1]
        # As we want to avoid any filename craziness,
        # folder names will be restricted to lowercase ascii letters,
        # numbers, and dashes:
        title = "".join(ch for ch in title if ch.isalnum() or ch == ' ')
        shortname = title.replace(' ', '-').lower()

        if not shortname:
            print_err("Could not auto-deduce shortname.")
            print_err("Please provide one explicitly.")
            exit(-1)

        folder = get_folder(shortname)

    # now, lets create the folder for the feed
    if os.path.exists(folder):
        print_err(f"Folder '{folder}' already exists. Try using a different shortname.")
        exit(-1)
    else:
        os.makedirs(folder)

    # we have successfully generated a folder that we can store the files in
    # trawl all the entries, and find links to audio files.
    feed["episodes"] = sorted_episodes(episodes_from_feed(parsed_feed))
    feed["shortname"] = shortname
    feed["title"] = parsed_feed["feed"]["title"]
    feed["url"] = url
    # write the configuration to a feed.json within the folder
    feed_file = get_feed_file(shortname)

    with open(feed_file, 'x') as f:
        json.dump(feed, f, indent=4)

    print(
        f"Imported {Fore.GREEN}{feed['title']}{Fore.RESET} with shortname {Fore.BLUE}{feed['shortname']}{Fore.RESET}.")


def update_feed(feed):
    """
    Download the current feed, and insert previously unknown
    episodes into our local config.
    """
    parsed_feed = feedparser.parse(feed["url"])

    for episode in episodes_from_feed(parsed_feed):
        already_got_this_ep = False

        for old_episode in feed["episodes"]:
            if episode["published"] == old_episode["published"] and episode["title"] == old_episode["title"]:
                already_got_this_ep = True
                break
        # only append new episodes!
        if already_got_this_ep:
            continue

        feed["episodes"].append(episode)
        print(f"New episode: '{episode['title']}'.")

    feed["episodes"] = sorted_episodes(feed["episodes"])

    overwrite_feed_config(feed)


def overwrite_feed_config(feed):
    """
    After updating the feed, or downloading new items,
    we want to update our local config to reflect that fact.
    """
    filename = get_feed_file(feed["shortname"])

    with open(filename, 'w') as f:
        json.dump(feed, f, indent=4)


def episodes_from_feed(parsed_feed):
    mimetypes = CONFIGURATION["mimetypes"]
    episodes = []

    for entry in parsed_feed.entries:
        # convert publishing time to unix time, so that we can sort
        # this should be unix time, barring any timezone shenanigans
        try:
            date = mktime(entry.published_parsed)
        except TypeError as e:
            print(f"TypeError: {e}")
            continue

        if not hasattr(entry, "links"):
            continue

        for link in entry.links:
            if not hasattr(link, "type"):
                continue

            if hasattr(link, "type") and (link.type in mimetypes):
                if hasattr(entry, "title"):
                    episode_title = entry.title
                else:
                    episode_title = link.href

                episodes.append(
                    {
                        "title": episode_title,
                        "url": link.href,
                        "downloaded": False,
                        "listened": False,
                        "published": date
                    }
                )

    return episodes


def download_multiple(feed, maxnum):
    for episode in feed["episodes"]:
        if maxnum == 0:
            break
        if not episode["downloaded"] and not episode_too_old(episode, CONFIGURATION["maxage-days"]):
            episode["filename"] = download_single(feed["shortname"], episode["url"])
            episode["downloaded"] = True
            maxnum -= 1

    overwrite_feed_config(feed)


def download_by_number(feed, num):
    if num > len(feed["episodes"]) or num < 1:
        print_red("This episode number was not found in given feed!")
        return

    episode = feed["episodes"][num - 1]

    if episode["downloaded"]:
        print_green(f"Episode '{episode['title']}' already downloaded, nothing to download.")
        return

    episode["filename"] = download_single(feed["shortname"], episode["url"])
    episode["downloaded"] = True

    overwrite_feed_config(feed)


def download_single(folder: str, url: str):
    print(url)

    base = CONFIGURATION["podcast-directory"]
    r = requests.get(url.strip(), stream=True)

    try:
        filename_lookup = re.findall('filename="([^"]+)', r.headers["content-disposition"])
    except KeyError:
        print("Inline data not provided, downloading from URL ...")
        filename_lookup = None

    if not filename_lookup:
        filename = get_filename_from_url(url)
    else:
        filename = filename_lookup[0]

    print_green("{:s} downloading...".format(filename))

    with open(os.path.join(base, folder, filename), "wb") as file:
        for chunk in r.iter_content(chunk_size=1024 ** 2):
            file.write(chunk)

    print_green("Done.")

    return filename


def available_feeds():
    """
    PodCat will save each feed to its own folder. Each folder should
    contain a json configuration file describing which elements
    have been downloaded already, and how many will be kept.
    """
    base = CONFIGURATION["podcast-directory"]
    paths = [
        p for p in os.listdir(base)
        if os.path.isdir(get_folder(p)) and os.path.isfile(get_feed_file(p))
    ]
    # For every folder, check if a configuration file exists.
    results = []

    for shortname in paths:
        with open(get_feed_file(shortname), 'r') as file:
            feed = json.load(file)
            results.append(feed)

    return sorted(results, key=lambda k: k["title"])


def find_feed(shortname):
    """
    All feeds are identified by their shortname, which is also the name of
    the folder they will be stored in.
    This function will find the correct folder, and parse the json file
    within that folder to generate the feed data.
    """
    feeds = available_feeds()

    for feed in feeds:
        if feed["shortname"] == shortname:
            return feed

    return None


def rename(shortname, new_name):
    folder = get_folder(shortname)
    new_folder = get_folder(new_name)

    if not os.path.isdir(folder):
        print_err(f"Folder {folder} not found.")
        exit(-1)

    os.rename(folder, new_folder)
    feed = find_feed(shortname)
    feed["shortname"] = new_name

    overwrite_feed_config(feed)


def prune_by_age(feed, max_age=0):
    shortname = feed["shortname"]
    episodes = feed["episodes"]

    for i, episode in enumerate(episodes):
        if not episode["downloaded"] or not episode_too_old(episode, max_age):
            continue

        episode_path = os.path.join(
            get_folder(shortname),
            episode.get("filename", get_filename_from_url(episode["url"]))
        )
        try:
            os.remove(episode_path)
        except OSError:
            print(f"Unable to remove file ({episode_path}) for episode: '{episode['title']}'.")
        else:
            print_red(f"Removed episode '{episode['title']}'.")
            episodes[i]["downloaded"] = False

    overwrite_feed_config(feed)


def prune_by_number(feed: dict, num: int):
    shortname = feed["shortname"]
    episode = feed["episodes"][num - 1]
    episode_path = os.path.join(get_folder(shortname), episode["filename"])

    if not episode["downloaded"]:
        print_green(f"Episode '{episode['title']}' not downloaded, nothing to delete.")
        return

    try:
        os.remove(episode_path)
    except OSError:
        print(f"Unable to remove file ({episode_path}) for episode: '{episode['title']}'.")
    else:
        print_red(f"Removed episode '{episode['title']}'.")
        episode["downloaded"] = False

    overwrite_feed_config(feed)


def pretty_print_feeds(feeds):
    format_str = Fore.GREEN + "  {0:24.45} {1:0}  |" + Fore.BLUE + "  {2:40}" + Fore.RESET + Back.RESET

    print()
    print(format_str.format("Title", "Episodes downloaded", "Shortname"))
    print_green(" " + "=" * 81)

    for feed in feeds:
        format_str = Fore.GREEN + "  {0:40.40} {1:3d}  |" + Fore.BLUE + "  {3:40}" + Fore.RESET + Back.RESET
        amount = len([ep for ep in feed["episodes"] if ep["downloaded"]])
        print(format_str.format(feed["title"], amount, '*', feed["shortname"]))


def pretty_print_episodes(feed):
    format_str = Fore.YELLOW + "{0:4})  | " + Fore.GREEN + "{1:40}  |  {2:20}" + Fore.RESET + Back.RESET

    print()

    for i, ep in enumerate(feed["episodes"]):
        status = Fore.RED + "Downloaded" if ep["downloaded"] else Fore.BLUE + "Not Downloaded"
        print(format_str.format(i + 1, ep["title"][:40], status))


def load_user_configuration(config_file: str) -> dict:
    config_file = expanduser(config_file)
    user_config = {}

    try:
        with open(config_file, "r") as file:
            user_config = json.load(file)
    except ValueError:
        print("invalid json in configuration file.")
        exit(-1)
    except FileNotFoundError:
        print("NOTE: No user configuration file found or provided, using default configuration.")

    return user_config


def main():
    colorama.init()
    arguments = docopt(__doc__, version="p0d 0.01")

    # before we do anything with the commands, find the configuration file
    user_configuration = load_user_configuration(arguments["--config"])

    CONFIGURATION.update(user_configuration)
    CONFIGURATION["podcast-directory"] = expanduser(CONFIGURATION["podcast-directory"])

    # handle the commands
    # --- Import new feed ---
    if arguments["import"]:
        if arguments["<shortname>"] is None:
            import_feed(arguments["<feed-url>"])
        else:
            import_feed(arguments["<feed-url>"], shortname=arguments["<shortname>"])
        exit(0)

    # --- List imported feeds ---
    if arguments["feeds"]:
        pretty_print_feeds(available_feeds())
        exit(0)

    # --- List episodes in feed(s) ---
    if arguments["episodes"]:
        feed = find_feed(arguments["<shortname>"])
        if feed:
            pretty_print_episodes(feed)
            exit(0)
        else:
            print_err(f"Feed '{arguments['<shortname>']}' not found.")
            exit(-1)

    # --- Update feed(s) ---
    if arguments["update"]:
        if arguments["<shortname>"]:
            feed = find_feed(arguments["<shortname>"])
            if feed:
                print_green(f"Updating '{feed['title']}'...")
                update_feed(feed)
                print_green("Done.")
            else:
                print_err(f"Feed '{arguments['<shortname>']}' not found.")
                exit(-1)
        else:
            for feed in available_feeds():
                print_green(f"Updating '{feed['title']}'...")
                update_feed(feed)
                print_green("Done.")
        exit(0)

    # --- Download episodes from feed(s) ---
    if arguments["download"]:
        ep_number = None

        if arguments["--number"]:
            ep_number = int(arguments["--number"])
        if arguments["--how-many"]:
            maxnum = int(arguments["--how-many"])
        else:
            maxnum = CONFIGURATION["maxnum"]

        # download episodes for a specific feed
        if arguments["<shortname>"]:
            feed = find_feed(arguments["<shortname>"])

            if not feed:
                print_err(f"Feed {arguments['<shortname>']} not found.")
                exit(-1)

            if ep_number is not None:
                download_by_number(feed, ep_number)
            else:
                download_multiple(feed, maxnum)
        # download episodes for all feeds.
        else:
            for feed in available_feeds():
                download_multiple(feed, maxnum)
        exit(0)

    # --- Rename feed ---
    if arguments["rename"]:
        rename(arguments["<shortname>"], arguments["<new_name>"])
        exit(0)

    # --- Prune/remove old episodes ---
    if arguments["prune"]:
        ep_number = None

        if arguments["--number"]:
            ep_number = int(arguments["--number"])
        if arguments["--maxage-days"]:
            max_age = int(arguments["--maxage-days"])
        else:
            max_age = CONFIGURATION.get("maxage-days", 0)

        # delete episodes from a specific feed
        if arguments["<shortname>"]:
            feed = find_feed(arguments["<shortname>"])

            if not feed:
                print_err(f"Feed '{arguments['<shortname>']}' not found.")
                exit(-1)

            if ep_number is not None:
                print_green(f"Pruning '{feed['title']}', episode #{ep_number}...")
                prune_by_number(feed, ep_number)
                print_green("Done.")
            else:
                print_green(f"Pruning '{feed['title']}'...")
                prune_by_age(feed, max_age)
                print_green("Done.")
        # delete episodes for all feeds
        else:
            for feed in available_feeds():
                print_green(f"Pruning '{feed['title']}'...")
                prune_by_age(feed, max_age)
                print_green("Done.")
        exit(0)


# if the application is called directly, like with the console command,
# then it will run as intended.
# If the application is imported as a module in another application, then
# it won't run.
if __name__ == "__main__":
    main()
