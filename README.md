# PodCat - podcatching for the terminal.
![podcat logo](https://codeberg.org/Almiky4527/PodCat/raw/branch/main/images/PodCat.png)

By a request from my friend - a repaired version of PodFox, a python tools for catching & managing podcast using the terminal.

Original project by brtmr on GitHub: https://github.com/brtmr/podfox/tree/master

## Installation & Requirements

This project is not yet available as a python library, so installation is purely done form here now.

You can copy/paste the `PodCat/podcat` folder to your python libraries directory (should work...).

With that being said, you need to install dependencies separately:

```
pip install colorama
pip install docopt
pip install feedparser
pip install requests
```

## List of features

PodCat is used identically to PodFox.

To start using PodCat, firstly you need to add podcast feed. This is done via command:

```py(thon) podcat import <feed-url> <feed-alias>```

Where `feed-url` is HTTP-based URL address of XML-styled feed, and `alias` is name to reference given feed with in other commands.

Next, you need to update given feed using `py(thon) podcat update <feed-alias>`.

You can list all imported feeds using `py(thon) podcat feeds` command. This will display feeds' titles, aliases, and number of downloaded
episodes from individual feeds.

To list episodes in given feed, use `py(thon) podcat episodes <feed-alias>`. This will list episodes' numbers, names, and download status.

To download episode with some ID, use `py(thon) podcat download <feed-alias> [--number=<episode-number>] [--how-many=<episodes-count>]`.
If given episode number exists, it is downloaded into directory specified by currently used configuration (see [user configuration](#user-configuration)).

You can always type `py podcat -h` for list of available commands.

## User Configuration

PodCat is configured using configuration file, by default located in `$HOME/podcat.json`.

If you want to use different configuration file from the one in this directory, 
use `-c=<filepath>` to explicitly specify desired configuration file.

Here is how a full user configuration file looks like, with all the parameters usable :

```
{
    "podcast-directory": "~/Podcasts",
    "maxnum":            1,
    "maxage-days":       365,
    "mimetypes":         [
        "audio/mp3",
        ...
    ]
}
```

### Remarks

`maxage-days` parameter is mainly used by the `prune` command, to determine how old episodes should be, to be valid for deletion.
It's also used by the `download` command, so old episodes are not re-downloaded.

Default value in PodCat is 365 days - 1 year.

That is also the reason to add the `--number` argument, to bypass this age checking.

But you may set `maxage-days` to whatever value you'd like in your user config file.